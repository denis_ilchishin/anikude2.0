import json, os

from django.utils import translation
from jinja2 import Environment
from django.urls import reverse, resolve, translate_url
from django.templatetags.static import static
from django.conf import settings

# from pprint import pprint,pformat
# from urllib import parse
# from datetime import datetime

# def chunk(value, size=4):
#     for i in range(0, len(value), size):
#         yield value[i:i + size]

# def get_static(path):
#     from django.contrib.staticfiles import finders

#     result = finders.find(path)

#     return open(result, encoding='utf-8').read()

# def turl(url, language_code, **kwargs):
#     return translate_url(url, language_code)
        
def jinjaEnv(**options):
    env = Environment(**options)
    env.trim_blocks = True
    env.lstrip_blocks = True
    env.globals.update({
        'url': reverse,
        'static': static,
        # 'translate_url': turl,
        # 'get_static': get_static,
        # 'chunk': chunk
    })
    return env
