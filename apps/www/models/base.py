import os, glob
from django.db import models
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.apps import apps
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import FileSystemStorage, cached_property, locks, File, setting_changed, file_move_safe, safe_join, filepath_to_uri, urljoin, datetime, timezone
from PIL import Image, ImagePalette
from django.core.files.images import get_image_dimensions


def validate_slug_unique(value, instance):
    if instance is not None:
        for model_name, model in apps.all_models['www'].items():
            for field in model._meta.fields:
                if isinstance(field, BaseSlugField):
                    if isinstance(instance, model):
                        exists = model.objects.exclude(id=instance.id).filter(slug=value).count()
                    else:
                        exists = model.objects.filter(slug=value).count()

                    if exists:
                        raise ValidationError(
                            '%(value)s - значение должно быть уникальным на всю систему!',
                            params={'value': value},
                        )


def title_image_path(instance, filename):
    """
        Build a path for title image to be upload in
    """

    if not instance.id:
        instance.save()
        
    return os.path.join(instance.__class__.__name__.lower(), str(instance.id), 'image.{}'.format(filename.split('.')[-1]))


def title_images_path(instance, filename):
    """
        Build a path for title images to be upload in
    """
    if not instance.title.id:
        instance.title.save()

    return os.path.join(instance.title.__class__.__name__.lower(), str(instance.title.id), 'image.{}'.format(filename.split('.')[-1]))


class BaseManager(models.Manager):
    def get(self, *args, **kwargs):
        try:
            return super().get(*args, **kwargs)
        except ObjectDoesNotExist:
            return None


class BaseModel(models.Model):
    objects = BaseManager()

    class Meta: 
        abstract = True


class BaseSlugField(models.SlugField):
    
    def __init__(self, *args, **kwargs):
        self.default_validators.append(validate_slug_unique)
        super().__init__(null=False, *args, **kwargs)

    def run_validators(self, value, model_instance):
        if value in self.empty_values:
            return

        errors = []
        for v in self.validators:
            try:
                if v == validate_slug_unique:
                    v(value, instance=model_instance)
                else:
                    v(value)
            except ValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    e.message = self.error_messages[e.code]
                errors.extend(e.error_list)

        if errors:
            raise ValidationError(errors)

    def clean(self, value, model_instance):
        value = self.to_python(value)
        self.validate(value, model_instance)
        self.run_validators(value, model_instance)
        return value


class BaseImageFieldFile(models.fields.files.ImageFieldFile):

    def _require_file(self):
        if not self:
            pass
            # raise ValueError("The '%s' attribute has no file associated with it." % self.field.name)


    @property
    def path(self):
        self._require_file()
        return self.storage.path(self.name)


    def url(self, height:int=100, width:int=0):
        self._require_file()
        return self.storage.url(self.name, height, width)


    def save(self, name, content, save=True):
        name = self.generate_filename(self.instance, name)

        
        # Cleaning cache images
        old_path = str(self.instance.tracker.previous(self.field.name))
        self.storage.detele(old_path)
        dir_name, filename = os.path.split(old_path)
        for f in glob.glob(os.path.join(
                settings.IMAGES_CACHE_ROOT, 
                dir_name, 
                '{}_*.{}'.format(filename.split('.')[0], filename.split('.')[-1]))
            ):
            self.storage.detele(f)

        self.name = self.storage.save(name, content, max_length=self.field.max_length)
        setattr(self.instance, self.field.name, self.name)
        self._committed = True

        # Save the object because it has changed, unless save is False
        if save:
            self.instance.save()
    save.alters_data = True

    def generate_filename(self, instance, filename):
        """
        Apply (if callable) or prepend (if a string) upload_to to the filename,
        then delegate further processing of the name to the storage backend.
        Until the storage layer, all file paths are expected to be Unix style
        (with forward slashes).
        """
        if callable(self.field.upload_to):
            filename = self.field.upload_to(instance, filename)
        else:
            filename = self.field.generate_filename(self.instance, filename)

        return self.storage.generate_filename(filename)


class BaseImageField(models.ImageField):
    attr_class = BaseImageFieldFile


class BaseImageStorageClass(FileSystemStorage):
    def __init__(self, *args, **kwargs):
        super().__init__(location=settings.IMAGES_ROOT, base_url=settings.IMAGES_URL, *args, **kwargs)
    
    def safe_join(self, base, *paths):
        return self.clear(safe_join(base, *paths))
    
    def clear(self, path):
        return path.replace('\\', '/')

    def path(self, name):
        return self.clear(self.safe_join(self.location, name))

    def rel_path(self, path):
        return self.clear(path).replace(self.clear(self.location), '').strip('/')
    
    def detele(self, name):
        if(os.path.exists(self.path(self.rel_path(name)))):
            os.unlink(self.path(self.rel_path(name)))
        
    def url(self, name, image_height:int, image_width:int):
        if image_height <= 0:
                image_height = 100

        if image_width > 0:
            image_size = str(image_width) + 'x' + str(image_height)
        else:
            image_size = str(image_height)

        path = self.safe_join(self.location, name)

        if os.path.exists(path) and os.path.isfile(path):
            cached_path = self.safe_join(settings.IMAGES_CACHE_ROOT, name)
            filepath, filename = os.path.split(cached_path)
            resized_filename = ''.join(filename.split('.')[:-1])
            resized_path = self.safe_join(filepath, resized_filename + '_' + image_size + '.' + filename.split('.')[-1])

        else:
            path = settings.IMAGES_PLACEHOLDER
            placeholder_filename = os.path.split(settings.IMAGES_PLACEHOLDER)[1]
            resized_path = self.safe_join(settings.IMAGES_CACHE_ROOT, '{name}_{size}.{extension}'.format(name=placeholder_filename.split('.')[0], extension=placeholder_filename.split('.')[-1], size=image_size))

        if not os.path.exists(resized_path):
            with open(path, 'r+b') as f:
                with Image.open(f) as image:
                    new_image = image.resize([int(image.width * image_height / image.height), int(image_height)], Image.ANTIALIAS)

                    if image_width > 0:
                        crop_coor = [
                            int(new_image.width / 2 - image_width / 2),
                            int(new_image.height / 2 - image_height / 2),
                        ]

                        bg = Image.new('RGB', (image_width, image_height), (255, 255, 255))

                        bg.paste(new_image, (-crop_coor[0], -crop_coor[1]))

                        new_image = bg

                    if not os.path.exists(os.path.dirname(resized_path)):
                        os.makedirs(os.path.dirname(resized_path))
                        
                    new_image.save(resized_path)

        path = resized_path

        url = filepath_to_uri(self.rel_path(path))
        if url is not None:
            url = url.lstrip('/')
        return urljoin(self.base_url, url)

BaseImageStorage = BaseImageStorageClass()