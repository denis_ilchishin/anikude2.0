from .base import BaseModel, models, BaseSlugField

class Translator(BaseModel):
    name = models.CharField(max_length=255, verbose_name='name')
    slug = BaseSlugField()
