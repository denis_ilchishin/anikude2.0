from .base import BaseModel, models


class Translation(BaseModel):
    SOURCE_KODIK = 1
    SOURCES = (
        (SOURCE_KODIK, 'Kodik'),
    )

    title = models.ForeignKey('Title', on_delete=models.CASCADE, related_name='translations', related_query_name='translation', verbose_name='title')

    translator = models.ForeignKey('Translator', on_delete=models.CASCADE, verbose_name='Переводчик')
    token = models.CharField(max_length=255, verbose_name='Токен')
    source = models.IntegerField(choices=SOURCES, null=True)
