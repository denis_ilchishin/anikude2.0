from .base import BaseModel, models, BaseSlugField

class Status(BaseModel):
    name = models.CharField(max_length=255, verbose_name='name')
    slug = BaseSlugField()
