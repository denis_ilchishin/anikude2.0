from .base import BaseModel, models, BaseSlugField, BaseImageField, title_images_path, title_image_path, BaseImageStorage
from model_utils.tracker import ModelTracker


class Title(BaseModel):

    YEAR_SEASON = 0
    YEAR_SEASON_WINTER = 1
    YEAR_SEASON_SPRING = 2
    YEAR_SEASON_SUMMER = 3
    YEAR_SEASON_AUTUMN = 4

    YEAR_SEASONS = (
        (YEAR_SEASON, '---'),
        (YEAR_SEASON_WINTER, 'Зима'),
        (YEAR_SEASON_SPRING, 'Весна'),
        (YEAR_SEASON_SUMMER, 'Лето'),
        (YEAR_SEASON_AUTUMN, 'Осень'),
    )

    AGE = 0
    AGE_13 = 1
    AGES = (
        (AGE, '---'),
        (AGE_13, 'pg-13'),
    )

    SOURCE = 0
    SOURCE_MANGA = 1
    SOURCES = (
        (SOURCE, '---'),
        (SOURCE_MANGA, 'manga'),
    )
    
    TYPE = 0
    TYPE_TV = 1
    TYPE_OVA = 2
    TYPE_MOVIE = 3
    TYPES = (
        (TYPE, '---'),
        (TYPE_TV, 'TV'),
        (TYPE_OVA, 'OVA'),
        (TYPE_MOVIE, 'MOVIE'),
    )

    TRANSLATION_TYPE = 0
    TRANSLATION_TYPE_MONO = 1
    TRANSLATION_TYPES = (
        (TRANSLATION_TYPE, '---'),
        (TRANSLATION_TYPE_MONO, 'Odnogolos'),
    )

    name = models.CharField(max_length=255, verbose_name='название')
    slug = BaseSlugField()
    status = models.ForeignKey('Status', on_delete=models.SET_NULL, null=True, verbose_name='status')
    year = models.IntegerField(blank=True, default=0, verbose_name='year')
    year_season = models.IntegerField(choices=YEAR_SEASONS, default=YEAR_SEASON, verbose_name='year season')
    age = models.IntegerField(choices=AGES, default=AGE, verbose_name='Age')
    source = models.IntegerField(choices=SOURCES, default=SOURCE, verbose_name='SOurce')
    types = models.IntegerField(choices=TYPES, default=TYPE, verbose_name='Type')
    episodes_number = models.CharField(max_length=255, verbose_name='episodes number')
    translation_type = models.IntegerField(choices=TRANSLATION_TYPES, default=TRANSLATION_TYPE, verbose_name='TRANSLATION TYPE')
    season = models.SmallIntegerField(default=1, verbose_name='Season')
    poster = BaseImageField(upload_to=title_image_path, storage=BaseImageStorage, verbose_name='poster')

    tracker = ModelTracker()


class TitleImage(models.Model):
    title = models.ForeignKey('Title', on_delete=models.CASCADE, related_name='images', related_query_name='image')
    image = BaseImageField(upload_to=title_images_path, storage=BaseImageStorage, verbose_name='image')
