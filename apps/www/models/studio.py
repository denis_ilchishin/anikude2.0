from .base import BaseModel, models, BaseSlugField

class Studio(BaseModel):
    name = models.CharField(max_length=255, verbose_name='name')
    slug = BaseSlugField()