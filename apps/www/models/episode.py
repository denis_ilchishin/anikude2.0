from .base import BaseModel, models


class Episode(BaseModel):
    episode = models.PositiveIntegerField(default=1, verbose_name='episiode')
    translation = models.ForeignKey('Translation', on_delete=models.CASCADE, related_name='episodes', related_query_name='episode', verbose_name='trnaslation')
