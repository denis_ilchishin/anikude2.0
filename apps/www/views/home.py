from django.shortcuts import render
from web.components import AnikudeTemplateView
from django.conf import settings
from django.template.loader import render_to_string
from web.models import Episode, Title, Translation, Update
from django.db.models import QuerySet
from django.core.paginator import Paginator
from django.http.response import JsonResponse, HttpResponse


class HomeView(AnikudeTemplateView):

    template_name = "web/home/home.jinja"

    LATESTS_PER_PAGE = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # latests = Paginator(Update.objects.filter(translation__title__status=Title.STATUS_ENABLED).order_by('-date', '-translation__title__visits'), 20).get_page(1)
        latests = Paginator(Title.objects.filter(status=Title.STATUS_ENABLED).order_by('-date_added'), self.LATESTS_PER_PAGE).get_page(1)
        # pagination = render_to_string('web/pagination.jinja', {'page': latests, 'request': self.request})
        context['latests'] = render_to_string('web/home/latests.jinja', {'latests': latests})
        context['populars'] = Title.objects.filter(status=Title.STATUS_ENABLED).order_by('-visits')[0:24]
        return context

    @classmethod
    def latests(cls, request):
        result = {}
        latests = Paginator(Title.objects.filter(status=Title.STATUS_ENABLED).order_by('-date_added'), cls.LATESTS_PER_PAGE).get_page(request.GET.get('page'))
        # result['latests'] = ''.join([latest.render_to_latest() for latest in latests])

        return HttpResponse(render_to_string('web/home/latests.jinja', {'latests': latests}))
    
class OfflineView(AnikudeTemplateView):

    template_name = "web/home/offline.jinja"



