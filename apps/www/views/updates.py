from django.shortcuts import render
from web.components import AnikudeTemplateView
from django.conf import settings
from django.template.loader import render_to_string
from web.models import Episode, Title, Translation, Update
from django.db.models import QuerySet
from django.core.paginator import Paginator
from django.http.response import JsonResponse


class UpdatesView(AnikudeTemplateView):

    template_name = "web/updates/updates.jinja"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['updates'] = Paginator(Update.objects.filter(translation__title__status=Title.STATUS_ENABLED).order_by('-date', '-translation__title__visits'), 20).get_page(self.request.GET.get('page'))
        # pagination = render_to_string('web/pagination.jinja', {'page': updates, 'request': self.request})
        # context['updates'] = render_to_string('web/updates/updates.jinja', {'updates': updates})
        return context




