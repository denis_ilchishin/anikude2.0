import numpy as np
from urllib.parse import unquote
from django.shortcuts import render
from web.components import AnikudeTemplateView
from django.conf import settings
from django.template.loader import render_to_string
from web.models import Episode, Title, Translation, Update, Config, TitleSearchQuery
from django.db.models import QuerySet, Q
from django.core.paginator import Paginator
from django.http.response import JsonResponse, HttpResponse
from .not_found import NotFound


class SearchView(AnikudeTemplateView):

    template_name = "web/search/search.jinja"

    def get(self, request):
        self.request.GET = self.request.GET.copy()
        if not self.request.GET.get('q') or len(self.request.GET.get('q', '').strip()) < 3 or len(self.request.GET.get('q', '').strip()) > 255:
            self.request.GET.__setitem__('q', self.request.GET.get('q', '').strip())
            return NotFound(request)
        self.request.GET.__setitem__('q', self.request.GET.get('q', '').strip())

        return super().get(self, request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        titles = Paginator(NeuralNetwork().get_titles(self.request.GET.get('q')), 20).get_page(self.request.GET.get('page', 1))

        context['pagination'] = render_to_string('web/pagination.jinja', {'page': titles, 'request': self.request, 'query': self.request.GET.get('q')})
        context['titles'] = titles
        context['q'] = self.request.GET.get('q')

        return context

    @classmethod
    def search_backprop(cls, request):
        title_id = unquote(request.POST.get('title_id'))
        query = unquote(request.POST.get('q', ''))
        title = Title.objects.get(id=int(title_id))
        if query and title and len(query.strip()) >= 3 and len(query.strip()) <= 255:
            NeuralNetwork().backprop(query, title_id)
        return HttpResponse()


class NeuralNetwork:
    def __init__(self):
        search_weights = Config.get(key='search_weights') or {
            'name_matches': 0,
            'name_full_match': 0,
            'name_query_matches': 0,

            'name_en_matches': 0,
            'name_en_full_match': 0,
            'name_en_query_matches': 0,

            'alternative_names_matches': 0,
            'alternative_names_query_matches': 0,

            'tags_matches': 0,
            'tags_query_matches': 0,
            
            'rating_total': 0,
            'visits': 0,
        }

        self.weights_list = list(search_weights.keys())
        self.weights = list(search_weights.values())
    def __sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def __sigmoid_derivative(self, x):
        return x * (1 - x)
        
    def backprop(self, query, title_id):
        if title_id == None:
            return

        # db.execute("SELECT * FROM search_query WHERE product_id = {product_id} AND query = '{query}'".format(query=query, product_id=product_id))

        # if(db.fetchone()):
        #     db.execute("UPDATE search_query SET count = count + 1 WHERE product_id = {product_id} AND query = '{query}'".format(query=query, product_id=product_id))
        # else:
        #     db.execute("INSERT search_query SET count = 1, product_id = {product_id}, query = '{query}'".format(query=query, product_id=product_id))
        
        titles = self.get_titles(query)

        if(titles):
        
            choises_weights = []
            choises_result = []
            choises = []

            # Pass the training set through our neural network (a single neuron).
            for title in titles:
                if int(title['title'].id) == int(title_id):
                    choises_result.append(1)
                else:
                    choises_result.append(title['__nnweight'])

                choises_weights.append(title['__nnweight'])
                choises.append(title['__nn'])

            choises = np.array(choises)
            choises_result = np.array(choises_result).T
            output = np.array([choises_weights])

            # прямое распространение
            l0 = choises
            l1 = output
            
            # насколько мы ошиблись?
            l1_error = choises_result - l1
            # перемножим это с наклоном сигмоиды 
            # на основе значений в l1
            l1_delta = l1_error * self.__sigmoid_derivative(l1) # !!!

            # обновим веса
            self.weights += np.dot(l0.T,l1_delta[0]) # !!!
            weights = {}
            for i, weight in enumerate(self.weights_list):
                weights[weight] = self.weights[i]
           
            Config.update(key='search_weights', value=weights, encoded=True)

    def get_titles(self, query):

        titles = []

        result = Title.objects.filter(
            Q(status=Title.STATUS_ENABLED) & 
            (
                Q(name__icontains=query) | 
                Q(name_en__icontains=query) | 
                Q(tags__icontains=query) | 
                Q(alternative_names__icontains=query)
            )
        )

        for title in result:

            name_matches = 0
            name_en_matches = 0
            alternative_names_matches = 0
            alternative_names_query_matches = 0
            tags_matches = 0
            tags_query_matches = 0

            for subquery in query.split(' '):
                name_matches += title.name.upper().count(subquery.upper())
                name_en_matches += title.name_en.upper().count(subquery.upper())
                for alternative_name in title.alternative_names.split(','):
                    alternative_names_matches += alternative_name.upper().count(subquery.upper())
                for tag in title.tags.split(','):
                    tags_matches += tag.upper().count(subquery.upper())
            
            for alternative_name in title.alternative_names.split(','):
                alternative_names_query_matches += alternative_name.upper().count(query.upper())
            for tag in title.tags.split(','):
                tags_query_matches += tag.upper().count(query.upper())

            data = {
                'name_matches': name_matches,
                'name_full_match': 1 if title.name.upper() == query.upper() else 0,
                'name_query_matches': title.name.upper().count(query.upper()),

                'name_en_matches': name_en_matches,
                'name_en_full_match': 1 if title.name_en.upper() == query.upper() else 0,
                'name_en_query_matches': title.name_en.upper().count(query.upper()),

                'alternative_names_matches': alternative_names_matches,
                'alternative_names_query_matches': alternative_names_query_matches,

                'tags_matches': tags_matches,
                'tags_query_matches': tags_query_matches,
                
                'rating_total': title.rating_total,
                'visits': title.visits,
            }
            
            alist = []

            for weight_item in self.weights_list:
                alist.append(data[weight_item])

            title_data = {
                'title': title,
                '__nn': alist,
                '__nnweight': self.__sigmoid(np.dot(alist, self.weights))
            }

            titles.append(title_data)

        titles = sorted(titles, key=lambda k: -k['__nnweight']) 

        return titles