import json, re
from django.urls import reverse
from .not_found import NotFound
from time import time
from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from web.components import AnikudeTemplateView, autonow
from web.models import Title, Episode, User, TitleComment, TitleCommentRate, TitleRate
from django import forms 
from django.template.loader import render_to_string


class CommentForm(forms.Form):
    comment = forms.CharField(label='', widget=forms.Textarea, max_length=2000)
    # spoiler = forms.BooleanField(label='Спойлер', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.get('comment').widget.attrs.update({'class': 'form-control', 'placeholder': 'Текст комментария', 'rows': 5, 'aria-label':'Текст комментария'})
        # self.fields.get('spoiler').widget.attrs.update({'data-toggle': 'tooltip', 'title':'Пометить данный комментарий как спойлер', 'value': 1})

    def render(self, request, title):
        return render_to_string('web/title/_comment_form.jinja', {'form': self, 'request':request, 'title': title})

    def get_fields(self):
        return [self[name] for name, field in self.fields.items()]

class TitleView(AnikudeTemplateView):

    template_name = "web/title/title.jinja"

    def get(self, request, title, *args, **kwargs):
        
        title = Title.objects.get(slug=title, status=Title.STATUS_ENABLED)

        if title is None:
            return NotFound(request)
        else:
            self.title = title
            if request.user.is_authenticated:
                request.user.notifications.filter(translation__in=title.translations.all()).update(read=1)

        visited = request.session.get('visited', [])
        if self.title.id not in visited:
            self.title.visit()
            request.session['visited'] = visited + [self.title.id]

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['request'] = self.request
        context['title'] = self.title
        context['user_rate'] = TitleRate.objects.filter(title=self.title, user=self.request.user).first().rate if self.request.user.is_authenticated and TitleRate.objects.filter(title=self.title, user=self.request.user).count() else None
        context['title_ratings'] = []
        for rate, text in TitleRate.RATES:
            total = TitleRate.objects.filter(title=self.title, rate=rate).count()
            context['title_ratings'].append((rate, text, total))

        context['comment_form'] = CommentForm() 
        context['last_viewed_episode'] = {}
        for translation in self.title.translations.all():
            context['last_viewed_episode'][translation.id] = translation.last_viewed_episode(self.request)

        return context

    @classmethod
    def leave_comment(cls, request, title):
        result = {'error': 'Ошибка'}
        if not request.user.is_authenticated:
            result = {'error': 'Необходимо авторизоваться'}
        else:
            additional = ''
            title = Title.objects.get(slug=title)
            form = CommentForm(request.POST)

            if form.is_valid() and request.user.is_authenticated and title and request.POST.get('comment'):
                spoiler = True if request.POST.get('spoiler', 0) else False
                comment = TitleComment(title=title, user=request.user, text=request.POST.get('comment'), spoiler=spoiler)
                
                comment.save()

                for username in list(set(re.findall(r'@([^,. ]+)[,. ]', request.POST.get('comment')))):
                    user = User.objects.get(username=username)
                    if not user:
                        additional = additional + '<br>Пользователь с ником "{}" не найден'.format(user.username)
                    elif request.user.id is user.id:
                        pass
                    elif comment.title.comments.filter(user_id=user.id).count():
                        comment.replied_to.add(user)
                    else:
                        additional = additional + '<br>Пользователь с ником "{}" не оставлял комментариев'.format(user.username)

                result = {'success': 'Комментарий успешно добавлен' + additional, 'comment': comment.render(request) if comment.status == comment.STATUS_ENABLED else None}
        return JsonResponse(result)
    
    @classmethod
    def comments(cls, request, title):
        result = {'error': 'Ошибка'}
        title = Title.objects.get(slug=title)
        sort = request.GET.get('sort', 'date') if request.GET.get('sort', 'date') else 'date'
        page = int(request.GET.get('page'))

        if title and sort in ['date', 'likes']:
            result = {'comments': title.render_comments(request, page, '-' + sort), 'has_next': title._get_comments(page).has_next()}
        return JsonResponse(result)
        
    @classmethod
    def rate_comment(cls, request, title):
        result = {'error': 'Ошибка'}
        comment = TitleComment.objects.get(id=request.POST.get('comment'))
        action = request.POST.get('action')
        if action in ['like', 'dislike']:
            text = 'Лайк' if action == 'like' else 'Дизлайк'

            if not request.user.is_authenticated:
                result = {'error': 'Необходимо авторизоваться'}
            elif comment and comment.user == request.user:
                result = {'error': 'Вы не можете поставить {} самому себе'.format(text)}
            elif comment and comment.rates.filter(user_id=request.user.id).count():
                comment_rate = comment.rates.filter(user_id=request.user.id).first()
                if comment_rate.rate is getattr(TitleCommentRate, 'RATE_' + action.upper()):
                    result = {'error': '{} уже поставлен'.format(text)}
                else:
                    if action == 'like':
                        comment_rate.rate = comment_rate.RATE_LIKE
                    else:
                        comment_rate.rate = comment_rate.RATE_DISLIKE

                    comment_rate.date = autonow()
                    comment_rate.save()
                    result = {
                        'success': '{} успешно поставлен'.format(text),
                        'likes': comment.rates.filter(rate=TitleCommentRate.RATE_LIKE).count(),
                        'dislikes': comment.rates.filter(rate=TitleCommentRate.RATE_DISLIKE).count()
                    }

            elif comment:
                comment_rate = TitleCommentRate(user=request.user, comment=comment, rate=getattr(TitleCommentRate, 'RATE_' + action.upper()))
                comment_rate.save()
                result = {
                    'success': '{} успешно поставлен'.format(text), 
                    'likes': comment.rates.filter(rate=TitleCommentRate.RATE_LIKE).count(),
                    'dislikes': comment.rates.filter(rate=TitleCommentRate.RATE_DISLIKE).count()
                }
                
        return JsonResponse(result)
    
    @classmethod
    def rate_title(cls, request, title):
        result = {'error': 'Ошибка'}
        if not request.user.is_authenticated:
            result = {'error': 'Необходимо авторизоваться'}
        else:
            title = Title.objects.get(slug=title)
            rate = request.POST.get('rate')
            
            if rate and TitleRate.is_valid_rate(rate) and title:
                if title.title_ratings.filter(user=request.user, rate=rate).count():
                    result = {'error': 'Оценка "{}" уже поставлена'.format(TitleRate.get_rating_text_by_value(rate))}
                else:
                    if title.title_ratings.filter(user=request.user).count():
                        title_rating = title.title_ratings.filter(user=request.user).first()
                        title_rating.rate = rate
                    else:
                        title_rating = TitleRate(user=request.user, rate=rate, title=title)

                    title_rating.save()

                    result = {'success': 'Оценка "{}" успешно поставлена'.format(TitleRate.get_rating_text_by_value(rate)),}
                
        return JsonResponse(result)
        