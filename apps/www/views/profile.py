import json, os, time
from django.shortcuts import render, redirect, HttpResponse
from web.components import AnikudeTemplateView, AnikudeImageStorage, add_notice, autonow
from django import forms
from django.template.loader import render_to_string
from django.forms.widgets import Input, Select
from web.models import User, Title, Episode, UserViewed, Translation, UserSubscription, UserNotification,UserEmailVerification, UserPasswordReset
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate, login, logout, backends
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from PIL import Image, ImagePalette
from django.urls import reverse
from django.http.response import JsonResponse
from .not_found import NotFound


""" Form fields widgets """
class ProfileFormEmailFieldWidget(Input):
    input_type = 'email'
    template_name = 'web/profile/_field.jinja'

class ProfileFormPasswordFieldWidget(Input):
    input_type = 'password'
    template_name = 'web/profile/_field.jinja'

    def __init__(self, attrs=None, render_value=False):
        super().__init__(attrs)
        self.render_value = render_value

    def get_context(self, name, value, attrs):
        if not self.render_value:
            value = None
        return super().get_context(name, value, attrs)

class ProfileFormCharFieldWidget(Input):
    input_type = 'text'
    template_name = 'web/profile/_field.jinja'

class ProfileFormChoiseFieldWidget(Select):
    template_name = 'web/profile/_select.jinja'

class ProfileFormDateFieldWidget(Input):
    input_type = 'date'
    template_name = 'web/profile/_field.jinja'

def password_validator(value):
    pass

######################################################################################################
class ProfileView(AnikudeTemplateView):

    template_name = "web/profile/profile.jinja"

    @classmethod
    def logout(cls, request, *args, **kwargs):

        if request.user.is_authenticated:
            logout(request)

        return redirect('home')

    def get(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            return redirect('login')

        return super().get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        return context

    @classmethod
    def viewed(cls, request):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)

        episode = Episode.objects.get(pk=request.POST.get('ep', 0))
        if episode is not None:
            if request.user.is_authenticated:
                UserViewed(episode=episode, user=request.user).save()
            else: 
                viewed = request.session.get('viewed', {})
                viewed[episode.translation.title.id] = episode.id
                request.session['viewed'] = viewed

            result = {'success': 'Успешно'}

        return HttpResponse(content=json.dumps(result), content_type='application/json')
    
    @classmethod
    def update_player_time(cls, request):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)
            
        if request.user.is_authenticated:
            viewed = UserViewed.objects.filter(episode_id=request.POST.get('ep', 0), user_id=request.user.id).order_by('-date')[0]
            if viewed is not None:
                viewed.player_time = request.POST.get('player_time', 0)
                viewed.date = autonow()
                viewed.save()
                result = {'success': 'Успешно'}

        return HttpResponse(content=json.dumps(result), content_type='application/json')

    @classmethod
    def add_to_list(cls, request):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)
            
        if not request.user.is_authenticated:
            result = {'error': 'Авторизуйтесь'}
        else:
            user_list = request.POST.get('list')
            if request.POST.get('title') and user_list in User.get_user_lists():
                ulist = getattr(request.user, user_list)
                title = Title.objects.get(pk=request.POST.get('title'))
                if title is not None:
                    if ulist.filter(id=request.POST.get('title')).count():
                        ulist.remove(title)
                        result = {'success': 'Удалено из списка "{}"'.format(request.user._meta.get_field(user_list).verbose_name)}
                    else:
                        if user_list == 'watching': 
                            if request.user.wishes.filter(id=title.id).count() or request.user.lost.filter(id=title.id).count():
                                request.user.wishes.remove(title)
                                request.user.lost.remove(title)
                                request.user.finished.remove(title)
                                ulist.add(title)

                                result = {'success': 'Перемещено в список "<strong>{}</strong>"'.format(request.user._meta.get_field(user_list).verbose_name)}
                        
                        elif user_list == 'wishes':
                            if request.user.watching.filter(id=title.id).count() or request.user.lost.filter(id=title.id).count():
                                request.user.watching.remove(title)
                                request.user.lost.remove(title)
                                ulist.add(title)

                                result = {'success': 'Перемещено в список "<strong>{}</strong>"'.format(request.user._meta.get_field(user_list).verbose_name)}
                                
                        elif user_list == 'lost':
                            if request.user.watching.filter(id=title.id).count() or request.user.wishes.filter(id=title.id).count():
                                request.user.watching.remove(title)
                                request.user.wishes.remove(title)
                                request.user.finished.remove(title)
                                ulist.add(title)

                                result = {'success': 'Перемещено в список "<strong>{}</strong>"'.format(request.user._meta.get_field(user_list).verbose_name)}
                                
                        elif user_list == 'finished':
                            if request.user.watching.filter(id=title.id).count() or request.user.wishes.filter(id=title.id).count() or request.user.lost.filter(id=title.id).count() or request.user.wishes.filter(id=title.id).count():
                                request.user.watching.remove(title)
                                request.user.wishes.remove(title)
                                request.user.lost.remove(title)
                                ulist.add(title)

                                result = {'success': 'Перемещено в список "<strong>{}</strong>"'.format(request.user._meta.get_field(user_list).verbose_name)}
                                
                        else:
                            ulist.add(title)
                            result = {'success': 'Добавленно в "<strong>{}</strong>"'.format(request.user._meta.get_field(user_list).verbose_name)}
                        
                    if 'success' not in result:
                        ulist.add(title)
                        result = {'success': 'Добавленно в "<strong>{}</strong>"'.format(request.user._meta.get_field(user_list).verbose_name)}

        return HttpResponse(content=json.dumps(result), content_type='application/json')
    
    @classmethod
    def title_move_to_lost(cls, request):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)
            
        title_id = request.GET.get('title')

        if request.user.is_authenticated:
                title = Title.objects.get(pk=title_id)
                if title is None or not request.user.watching.filter(id=title_id).count():
                    result = {'error': 'Ошибка. Сериал не верный'}
                else:
                    Title.move_to_lost(request.user, title)

                    result = {
                        'success': '<strong><a href="{}">{}</a></strong> успешно перемещен в "<strong>{}</strong>"'.format(title.url, title.name, request.user._meta.get_field('lost').verbose_name),
                        'html': request.user.render_user_lists(active='watching')
                    }

        return HttpResponse(content=json.dumps(result), content_type='application/json')
        
    @classmethod
    def title_move_to_watching(cls, request):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)
            
        title_id = request.GET.get('title')

        if request.user.is_authenticated:
            title = Title.objects.get(pk=title_id)
            if title is None or not request.user.lost.filter(id=title_id).count():
                result = {'error': 'Ошибка. Сериал не верный'}
            else:
                Title.move_to_watching(request.user, title)
                
                result = {
                    'success': '<strong><a href="{}">{}</a></strong> успешно перемещен в "<strong>{}</strong>"'.format(title.url, title.name, request.user._meta.get_field('watching').verbose_name),
                    'html': request.user.render_user_lists(active='lost')
                }

        return HttpResponse(content=json.dumps(result), content_type='application/json')
        
    @classmethod
    def title_list_remove(cls, request):
        
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)

        user_list = request.POST.get('list')
        title_id = request.POST.get('title')

        if request.user.is_authenticated:
            if user_list in request.user.get_user_lists():
                title = Title.objects.get(pk=title_id)
                if title is None or (getattr(request.user, user_list) and not getattr(request.user, user_list).filter(id=title_id).count()):
                    result = {'error': 'Ошибка. Сериал не верный'}
                else:
                    Title.remove_from_list(request.user, user_list, title)
                    
                    result = {
                        'success': '<strong><a href="{}">{}</a></strong> успешно удален из списка "<strong>{}</strong>"'.format(title.url, title.name, request.user._meta.get_field(user_list).verbose_name),
                        'html': request.user.render_user_lists(active=user_list)
                    }

        return HttpResponse(content=json.dumps(result), content_type='application/json')
        
    @classmethod
    def translation_subscribe(cls, request):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)
        result = {'error': 'Авторизуйтесь'}
        if request.user.is_authenticated:
            translation = Translation.objects.get(id=request.POST.get('tr'))
            if translation:
                if request.user.subscriptions.filter(translation=translation).count():
                    request.user.subscriptions.filter(translation=translation).delete()
                    result = {'success': 'Отписка успешно выполнена'}
                else:
                    UserSubscription(user=request.user, translation=translation).save()
                    result = {'success': 'Подписка успешно оформлена'}

        return HttpResponse(content=json.dumps(result), content_type='application/json')
    
    @classmethod
    def nf_subscribe(cls, request):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)
        result = {'error': 'Авторизуйтесь'}
        if request.user.is_authenticated:
            pass
            # translation = Translation.objects.get(id=request.POST.get('tr'))
            # if translation:
            #     if request.user.subscriptions.filter(translation=translation).count():
            #         request.user.subscriptions.filter(translation=translation).delete()
            #         result = {'success': 'Отписка успешно выполнена'}
            #     else:
            #         UserSubscription(user=request.user, translation=translation).save()
            #         result = {'success': 'Подписка успешно оформлена'}

        return HttpResponse(content=json.dumps(result), content_type='application/json')

class ProfileEditView(AnikudeTemplateView):

    template_name = "web/profile/edit.jinja"

    def get(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            return redirect('login')

        return super().get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        context['change_password_form'] = getattr(self, 'change_password_form', None) or ChangePasswordForm()
        context['change_info_form'] = getattr(self, 'change_info_form', None) or ChangeInfoForm({'birth_date': self.request.user.birth_date, 'gender': self.request.user.gender})
        return context

    @classmethod
    def update_image(cls, request):
        image_max_width = 300
        image_max_height = 300

        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)
        if request.user.is_authenticated:
            file = request.FILES.get('new_image')
            pos = request.POST.get('pos')

            if file and file.content_type in AnikudeImageStorage.allowed_content_types and pos:
                crop_coor = [int(float(pos)) for pos in pos.split(',')]
                with file.open('r+b') as f:
                    with Image.open(f).convert('RGB') as image:

                        new_image = image.crop(crop_coor)
                        new_image = new_image.resize([min(image_max_width, new_image.width), min(image_max_height, new_image.height)])

                        new_filename = User._meta.get_field('image').save_filename + '.jpg'
                        new_path = request.user.image.storage.path(request.user.image.images_path(request.user, new_filename))
                        if not os.path.exists(os.path.split(new_path)[0]):
                            os.makedirs(os.path.split(new_path)[0])
                        new_image.save(new_path)
                        request.user.image = request.user.image.storage.rel_path(new_path)
                        request.user.image.clear_cache()
                        request.user.save()
                        result = {
                            'success': 'Аватар успешно обновлен',
                            'image': request.user.image.url(300)
                        }

        return HttpResponse(content=json.dumps(result), content_type='application/json')

    @classmethod
    def change_password(cls, request):
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            if request.POST.get('new_password') == request.POST.get('old_password'):
                form.add_error('new_password', 'Новый пароль не может совпадать со старым')
            elif request.POST.get('new_password_confirmation') != request.POST.get('new_password'):
                form.add_error('new_password_confirmation', 'Пароль введен неверно')
            elif not authenticate(username=request.user.email, password=request.POST.get('old_password')):
                form.add_error('old_password', 'Старый пароль введен неверно')

            if form.is_valid():
                request.user.set_password(request.POST.get('old_password'))
                add_notice(request.session, 'Пароль успешно обновлен', 'success')
                return redirect('profile_edit')

        view = cls()
        view.change_password_form = form
        view.request = request
        return view.get(request)
    
    @classmethod
    def change_info(cls, request):
        form = ChangeInfoForm(request.POST)
        if form.is_valid():
            request.user.birth_date = request.POST.get('birth_date')
            request.user.gender = request.POST.get('gender')
            request.user.save()
            add_notice(request.session, 'Данные профиля успешно обновлены', 'success')

        view = cls()
        view.change_info_form = form
        view.request = request
        return view.get(request)

    @classmethod
    def delete_image(cls, request):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)

        if request.user.is_authenticated:
            request.user.image = None
            request.user.save()
            result = {'success': 'Аватар успешно удален'}
        else:
            result = {'error': 'Авторизуйтесь'}

        return HttpResponse(content=json.dumps(result), content_type='application/json')


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(label='Старый пароль', max_length=255, widget=ProfileFormPasswordFieldWidget)
    new_password = forms.CharField(label='Новый пароль', max_length=255, widget=ProfileFormPasswordFieldWidget, validators=[password_validator])
    new_password_confirmation = forms.CharField(label='Подтверждение нового пароля', max_length=255, widget=ProfileFormPasswordFieldWidget)

    def get_fields(self):
        return [self[name] for name, field in self.fields.items()]

    def __str__(self):
        return render_to_string('web/profile/_form.jinja', {'form': self})

class ChangeInfoForm(forms.Form):
    birth_date = forms.CharField(label='Дата рождения', widget=ProfileFormDateFieldWidget, required=False)
    gender = forms.ChoiceField(label='Пол', choices=User.GENDERS, widget=ProfileFormChoiseFieldWidget)

    def get_fields(self):
        return [self[name] for name, field in self.fields.items()]

    def __str__(self):
        return render_to_string('web/profile/_form.jinja', {'form': self})

###############################################################################################################
class RegisterForm(forms.Form):
    email = forms.EmailField(label='Email', max_length=255, widget=ProfileFormEmailFieldWidget)
    username = forms.CharField(label='Никнейм', max_length=255, widget=ProfileFormCharFieldWidget)
    password = forms.CharField(label='Пароль', max_length=255, widget=ProfileFormPasswordFieldWidget, validators=[password_validator])
    password_confirmation = forms.CharField(label='Подтверждение пароля', max_length=255, widget=ProfileFormPasswordFieldWidget)

    def get_fields(self):
        return [self[name] for name, field in self.fields.items()]

    def __str__(self):
        return render_to_string('web/profile/_form.jinja', {'form': self})


class RegistrationView(AnikudeTemplateView):

    template_name = "web/profile/registration.jinja"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')

        self.form = RegisterForm()

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')

        self.form = RegisterForm(self.request.POST)

        if self.form.is_valid():
            
            try:
                backends.UserModel.objects.get(email=request.POST['email'])
                self.form.add_error('email', 'Пользователь с таким email уже существует')
            except ObjectDoesNotExist:
                pass
        
            try:
                backends.UserModel.objects.get(username=request.POST['username'])
                self.form.add_error('username', 'Пользователь с таким никнеймом уже существует')
            except ObjectDoesNotExist:
                pass

            if request.POST['password_confirmation'] != request.POST['password']:
                self.form.add_error('password_confirmation', 'Пароль введен не верно')

            if self.form.is_valid():
                user = backends.UserModel.objects.create_user(username=request.POST['username'], email=request.POST['email'], password=request.POST['password'])

                user.email_verification(request)
                return redirect('profile_verification')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form
        return context


#################################################################################################################
class LoginForm(forms.Form):
    email = forms.EmailField(label='Email', max_length=255, widget=ProfileFormEmailFieldWidget)
    password = forms.CharField(label='Пароль', max_length=255, widget=ProfileFormPasswordFieldWidget)

    def get_fields(self):
        return [self[name] for name, field in self.fields.items()]

    def __str__(self):
        return render_to_string('web/profile/_form.jinja', {'form': self})


class LoginView(AnikudeTemplateView):

    template_name = "web/profile/login.jinja"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')

        self.form = LoginForm()

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')

        self.form = LoginForm(self.request.POST)
        if self.form.is_valid():

            user = authenticate(request, username=request.POST['email'], password=request.POST['password'])

            if user is not None:
                if user.verified:
                    login(request, user)
                    return redirect('profile')
                else:
                    if user.email_verifications.all().count():
                        request.session['email_verification_id'] = user.email_verifications.all()[0].id
                    return redirect('profile_verification')
            else:
                self.form.validation_error = 'Пользователь с таким email не найден или пароль введен неверно. Проверьте данные и попробуйте еще раз'

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form
        return context

class NotificationView(AnikudeTemplateView):
    template_name = 'web/profile/notifications.jinja'

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('profile')
        return super().get(request=request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('profile')
        return super().get(request=request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['notifications'] = self.request.user.notifications.order_by('-date').all()
        return context

    @classmethod
    def notification_read(cls, request, *args, **kwargs):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)

        if request.user.is_authenticated:
            notification = request.user.notifications.get(id=request.POST.get('id'))
            if notification:
                notification.read = 1
                notification.save()
                result = {'success': 'Уведомление успешно помечено как прочитано', 'total_unread_left': request.user.notifications.filter(read=0).count()}
        else:
            result = {'error': 'Авторизуйтесь'}

        return JsonResponse(result)

    @classmethod
    def notification_delete(cls, request, *args, **kwargs):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)
            
        if request.user.is_authenticated:
            notification = request.user.notifications.get(id=request.POST.get('id'))
            if notification:
                notification.delete()
                result = {'success': 'Уведомление успешно удалено', 'total_unread_left': request.user.notifications.filter(read=0).count(), 'total_left': request.user.notifications.count()}
        else:
            result = {'error': 'Авторизуйтесь'}

        return JsonResponse(result)

    @classmethod
    def notification_read_all(cls, request, *args, **kwargs):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)
            
        if request.user.is_authenticated:
            request.user.notifications.all().update(read=1)
            result = {'success': 'Все уведомления успешно помечено как прочитано'}
        else:
            result = {'error': 'Авторизуйтесь'}

        return JsonResponse(result)

    @classmethod
    def notification_delete_all(cls, request, *args, **kwargs):
        result = {'error': 'Ошибка'}
        if request.method != 'POST':
            return JsonResponse(result)

        if request.user.is_authenticated:
            request.user.notifications.all().delete()
            result = {'success': 'Все уведомления успешно удалены'}
        else:
            result = {'error': 'Авторизуйтесь'}

        return JsonResponse(result)


class UserProfileView(AnikudeTemplateView):
    template_name = "web/profile/user_profile.jinja"

    def get(self, request, id, *args, **kwargs):

        self.user = User.objects.get(id=id)

        if not self.user:
            return NotFound(request)
        elif request.user.is_authenticated and request.user == self.user:
            return redirect('profile')

        return super().get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_profile'] = self.user
        return context


class VerificationView(AnikudeTemplateView):

    template_name = 'web/profile/verification/info.jinja'

    def get(self, request, *args, **kwargs):
        user = request.session.get('email_verification_id')

        if not user:
            if self.request.user.is_authenticated:
                return redirect('profile')
            else:
                return redirect('home')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['email_verification'] = UserEmailVerification.objects.get(id=self.request.session.get('email_verification_id'))

        return context
    
    @classmethod
    def send_again(cls, request):
        result = {'error': 'Произошла ошибка'}

        if request.method == 'POST' and request.session.get('email_verification_id'):
            email_verification = UserEmailVerification.objects.get(id=request.session.get('email_verification_id'))
            if int(time.time()) - email_verification.date >= User.EMAIL_VERIFICATION_TIMEOUT_EXPIRE:
                email_verification.user.email_verification(request)
                result = {'success': 'Письмо успешно отправленно'}
            else:
                result = {'error': 'Вы сможете отправить письмо повторно через {} секунд'.format(User.EMAIL_VERIFICATION_REQUEST_TIMEOUT - (int(time.time() - email_verification.date))) }
        return JsonResponse(result)


class VerificationConfirmView(AnikudeTemplateView):

    template_name = 'web/profile/verification/success.jinja'

    def get(self, request, token, *args, **kwargs):
        email_verification = UserEmailVerification.objects.get(token=token)

        if email_verification:
            email_verification.user.verified = 1
            email_verification.user.save()
            login(request, email_verification.user)
            self.email_verification = email_verification
            email_verification.delete()
        else:
            return redirect('profile')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['email_verification'] = self.email_verification

        return context


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label='Email', max_length=255, widget=ProfileFormEmailFieldWidget)

    def get_fields(self):
        return [self[name] for name, field in self.fields.items()]

    def __str__(self):
        return render_to_string('web/profile/_form.jinja', {'form': self})

        
class PasswordResetView(AnikudeTemplateView):

    template_name = 'web/profile/password_reset/reset.jinja'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')

        self.form = PasswordResetForm()

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')

        self.form = PasswordResetForm(request.POST)
        if self.form.is_valid():

            if User.objects.get(email=request.POST.get('email')):
                User.objects.get(email=request.POST.get('email')).password_reset(request)
                return redirect('password_reset_info')
            else:
                self.form.validation_error = 'Пользователь с таким email не найден. Проверьте данные и попробуйте еще раз'

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form

        return context


class PasswordResetInfoView(AnikudeTemplateView):

    template_name = 'web/profile/password_reset/info.jinja'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')
        elif not request.session.get('password_reset_id'):
            return redirect('password_reset')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['password_reset'] = UserPasswordReset.objects.get(id=self.request.session['password_reset_id'])

        return context

    @classmethod
    def send_again(cls, request):
        result = {'error': 'Произошла ошибка'}

        if request.method == 'POST' and request.session.get('password_reset_id'):
            password_reset = UserPasswordReset.objects.get(id=request.session.get('password_reset_id'))
            if int(time.time()) - password_reset.date >= User.PASSWORD_RESET_REQUEST_TIMEOUT:
                password_reset.user.password_reset(request)
                result = {'success': 'Письмо успешно отправленно'}
            else:
                result = {'error': 'Вы сможете отправить письмо повторно через {} секунд'.format(User.EMAIL_VERIFICATION_REQUEST_TIMEOUT - (int(time.time() - password_reset.date))) }
        return JsonResponse(result)


class PasswordResetConfirmView(AnikudeTemplateView):

    template_name = 'web/profile/password_reset/confirm.jinja'

    def get(self, request, token, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')

        password_reset = UserPasswordReset.objects.get(token=token)
        if not password_reset or int(time.time()) - password_reset.date >  User.PASSWORD_RESET_REQUEST_EXPIRE:
            return redirect('password_reset_fail')

        request.session['password_reset_id'] = password_reset.id

        self.password_reset = password_reset

        self.form = PasswordResetConfirmForm()

        return super().get(request, *args, **kwargs)

    def post(self, request, token, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')
        
        password_reset = UserPasswordReset.objects.get(token=token)
        if not password_reset or int(time.time()) - password_reset.date >  User.PASSWORD_RESET_REQUEST_EXPIRE:
            return redirect('password_reset_fail')
            
        self.password_reset = password_reset

        self.form = PasswordResetConfirmForm(request.POST)
        
        if self.form.is_valid():
            if request.POST['password_confirmation'] != request.POST['password']:
                self.form.add_error('password_confirmation', 'Пароль введен не верно')

            if self.form.is_valid():
                user = password_reset.user
                user.set_password(request.POST['password'])
                user.save()
                password_reset.delete()
                return redirect('password_reset_success')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form
        context['password_reset'] = self.password_reset

        return context


class PasswordResetConfirmForm(forms.Form):
    password = forms.CharField(label='Новый пароль', max_length=255, widget=ProfileFormPasswordFieldWidget, validators=[password_validator], required=True)
    password_confirmation = forms.CharField(label='Подтверждение пароля', max_length=255, widget=ProfileFormPasswordFieldWidget, required=True)

    def get_fields(self):
        return [self[name] for name, field in self.fields.items()]

    def __str__(self):
        return render_to_string('web/profile/_form.jinja', {'form': self})



class PasswordResetSuccessView(AnikudeTemplateView):

    template_name = 'web/profile/password_reset/success.jinja'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')
        elif not request.session.get('password_reset_id'):
            return redirect('password_reset')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        del self.request.session['password_reset_id']

        return context


class PasswordResetFailView(AnikudeTemplateView):

    template_name = 'web/profile/password_reset/fail.jinja'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('profile')
        elif not request.session.get('password_reset_id'):
            return redirect('password_reset')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context