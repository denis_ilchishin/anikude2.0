from django.shortcuts import render
from web.components import AnikudeTemplateView

def NotFound(request):
    template_name = "web/404.jinja"
    view = AnikudeTemplateView(request=request)
    return render(request, template_name, context=view.get_context_data(), status=404)
