import json
from django.shortcuts import render, render_to_response
from django.template.loader import render_to_string
from web.models import Title, Genre, Category, Studio, Translator
from web.components import AnikudeTemplateView
from django.db.models import Sum, Count, F, FloatField, IntegerField
from django.core.paginator import Paginator

class Filter():
    
    FILTERS = [
        ['category', Category],
        ['genres', Genre],
        ['translation__translator', Translator],
        ['studios', Studio],
    ]

    SORTS = {
        '-rate': 'Рейтингу',
        '-year': 'Году выпуска',
        'name': 'Названию',
    }
    
    def __init__(self, request, **kwargs):
        self.request = request
        self.router_map = self._router_map()
        self.query = kwargs['filter'] if 'filter' in kwargs else ''
        self.decoded = self.decode()
        self.per_page = 24
        self.order_by = self.request.GET.get('sort') if self.request.GET.get('sort') in self.SORTS else '-rate'
        self.page = self._get_page()
    
    def decode(self):
        result = {}

        for f, obj in self.FILTERS:
            result[obj.__name__] = []

        if not self.query:
            return result
                
        if not self.check():
            return result

        for q in self.query.strip('/').split('/'):
            result[self.router_map[q].__class__.__name__].append(self.router_map[q].id)
    
        return result

    def check(self):
        if self.query:
            for q in self.query.strip('/').split('/'):
                if q not in self.router_map:
                    return False

        return True

    @property
    def is_page_valid(self):
        if not self.check() or int(self.request.GET.get('page', 1)) < 1 or int(self.request.GET.get('page', 1)) > self.page.num_pages:
            return False
        return True
        
    def _router_map(self):
        result = {}
        for f, obj in self.FILTERS:
            for instance in obj.objects.all():
                if instance.slug:
                    result[instance.slug] = instance

        return result

    def _get_page(self):
        titles = Title.objects
        if self.decoded:
            for field, obj in self.FILTERS:
                if self.decoded[obj.__name__]:
                    titles = titles.filter(**{field + '__in': self.decoded[obj.__name__]})

        # if self.request.GET.get('inf', False):
            return Paginator(titles.exclude(slug='').filter(status=Title.STATUS_ENABLED).annotate(Sum('title_rating__rate', output_field=FloatField())).annotate(Count('title_rating', output_field=FloatField())).annotate(rate=F('title_rating__rate__sum') / F('title_rating__count')).distinct().order_by(self.order_by, '-date_added'), self.per_page)
        # else:
            # page = Paginator(titles.exclude(slug='').filter(status=Title.STATUS_ENABLED).annotate(Sum('title_rating__rate', output_field=FloatField())).annotate(Count('title_rating', output_field=FloatField())).annotate(rate=F('title_rating__rate__sum') / F('title_rating__count')).distinct().order_by('-rate', '-date_added'), (int(self.per_page) * int(self.request.GET.get('page', 1))))
            # page.number = int(self.request.GET.get('page', 1))

            # return page

    def get_titles(self):
        # if self.request.GET.get('inf', False):
            return self.page.get_page(self.request.GET.get('page', 1))
        # else:
            # return self.page.get_page(1)

    @property
    def allow_index(self):
        if not self.has_filters():
            return True
        else:
            if Category.__name__ in self.decoded and self.has_filters() == 1 and len(self.decoded[Category.__name__]) == 1:
                return True
            else:
                return False     

    def pagination(self, page):
        return render_to_string('web/pagination.jinja', {'page': page, 'request': self.request, 'query': None,'sort': None if self.order_by == '-rate' else self.order_by})

    def render(self):
        return render_to_string('web/catalog/filter.jinja', {'filter': self})

    def has_filters(self):
        filters = 0
        for items in self.decoded.values():
            filters = filters + len(items)
        return filters


    def get_sorts(self):
        result = []
        for sort in self.SORTS:
            result.append({
                'value': sort,
                'label': self.SORTS[sort],
                'selected': True if self.order_by == sort else False
            })

        return result

    @property
    def meta_title(self):
        if not self.has_filters():
            return 'Каталог аниме | Anikude.com'
        else:
            if Category.__name__ in self.decoded and self.has_filters() == 1 and len(self.decoded[Category.__name__]) == 1:
                return '{} аниме | Anikude.com'.format(Category.objects.get(id=self.decoded[Category.__name__][0]).name)
            else:
                return 'Каталог аниме по фильтру и жанру | Anikude.com'
    
    @property
    def meta_description(self):
        if not self.has_filters():
            return 'Каталог аниме, смотреть весь список аниме на сайте | Anikude.com'
        else:
            if Category.__name__ in self.decoded and self.has_filters() == 1 and len(self.decoded[Category.__name__]) == 1:
                return '{} аниме смотреть онлайн | Anikude.com'.format(Category.objects.get(id=self.decoded[Category.__name__][0]).name)
            else:
                return 'Каталог аниме по фильтру и жанру | Anikude.com'
        
        
        
    # def meta_description(self):

class CatalogView(AnikudeTemplateView):

    template_name = 'web/catalog/catalog.jinja'

    def get(self, request, *args, **kwargs):
        self.filter = Filter(request, **kwargs)
        if not self.filter.is_page_valid:
            return self.not_found()

        if request.GET.get('filter'):
            self.template_name = 'web/catalog/catalog_filter.jinja'

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.filter
        context['allow_index'] = self.filter.allow_index
        context['titles'] = self.filter.get_titles()
        context['pagination'] = self.filter.pagination(context['titles'])
        context['request'] = self.request
        return context
