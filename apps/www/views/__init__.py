from .catalog import CatalogView
from .home import HomeView, OfflineView
from .profile import ProfileView, ProfileEditView, LoginView, RegistrationView, NotificationView, UserProfileView, VerificationView, VerificationConfirmView, PasswordResetView, PasswordResetConfirmView, PasswordResetInfoView, PasswordResetSuccessView, PasswordResetFailView
from .title import TitleView
from .search import SearchView
from .not_found import NotFound
from .updates import UpdatesView
