import json, os
from plural_ru import ru as plural  
from django import template
from jinja2 import Environment
from django.urls import reverse, resolve
from pprint import pprint,pformat
from urllib import parse
from datetime import datetime
from django.conf import settings
from math import floor

register = template.Library()

@register.filter
def chunk(value, size=4):
    for i in range(0, len(value), size):
        yield value[i:i + size]

@register.filter
def url(obj, size=None):
    return obj.url(size)

@register.simple_tag
def imgurl(obj, height=None, width=None):
    return obj.url(height, width)

from django.template import Context, Template

def ficon(icon, style_prefix='fa', **kwargs):
    t = Template('<i class="{{style_prefix}} {{style_prefix}}-{{icon}}"></i>')
    c = Context({'style_prefix': style_prefix, 'icon': icon})
    return t.render(c)

def pre(obj):
    if getattr(obj, '__dict__'):
        obj = obj.__dict__

    return '<pre>{string}</pre>'.format(string=pformat(obj))
    
def query(parameters):
    return parse.urlencode(parameters)

def format_date(timestamp, date_format='%H:%M %d {month} %Y'):
    date = datetime.utcfromtimestamp(timestamp)
    return date.strftime(date_format).format(month=settings.MONTHS[date.month])

def duration(seconds):
    minutes = int(floor(seconds/60))
    if minutes < 10:
        minutes = '0' + str(minutes)

    seconds = seconds % 60
    if seconds < 10:
        seconds = '0' + str(seconds)

    return '{}:{}'.format(minutes, seconds)

def get_static(path):
    return open(os.path.join(settings.BASE_DIR + path), encoding='utf-8').read()


from sass import compile as sass_compile

def jinjaEnv(**options):
    env = Environment(**options)
    env.trim_blocks = True
    env.lstrip_blocks = True
    env.globals.update({
        'url': reverse,
        'chunk': chunk,
        'f': ficon,
        'pre': pre,
        'plu': plural,
        'query': query,
        'json': json.dumps,
        'date': format_date,
        'duration': duration,
        'get_static': get_static
        # 'basecss': sass_compile(filename=settings.STATIC_ROOT + '/web/css/base.scss')
    })
    return env