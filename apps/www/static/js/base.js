function getUrlVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/sw.js').then((reg) => {
            console.log('[PWA] Service worker registered.', reg);
        });
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(document).ready(function(){
    // $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
});

var csrftoken = getCookie('csrftoken');

var noticeAudio = new Audio('/static/web/audio/notification.mp3');

var Notice = {
    query: [],

    notificating: false,

    add: (text, style='primary') => {
        time = new Date().getTime()

        var _class = 'text-' + style;
        var _header = '<i class="fa fa-1.5x fa-bell"></i>';
    
        $('body').append(`
            <div id="` + time + `" class="toast fade hide" data-autohide="true" data-delay="3000">
                <div class="toast-body">
                    <span class="toast-icon `+ _class +`">` + _header + `</span>` +  text + `
                </div>
            </div>
        `);
        
        Notice.query.push(time.toString());
    
        if(!Notice.notificating){
            Notice.proccess();
        }
    },

    proccess: () => {
        $.each($('.toast'), (i,e) => {
            if(!Notice.query.includes($(e).attr('id').toString())){
                $(e).removeClass('show');
                setTimeout(() => $(e).remove(), 1000);
            }
        });
    
        if (Notice.query.length){
    
            n = Notice.query.shift();
            $('#' + n).removeClass('hide').addClass('show');
            noticeAudio.volume = 0.25;
            noticeAudio.play();

            setTimeout(() => {
                Notice.proccess();
            }, 3300);
    
            Notice.notificating = true;
        }else{
            Notice.notificating = false;
        }
    }
}

// window.addEventListener('beforeinstallprompt', saveBeforeInstallPromptEvent);
function unloadPage(){
    $('.navbar-toggler .spinner-border').removeClass('d-none');
    $('.navbar-toggler .navbar-toggler-icon').addClass('d-none');
    return null;
}