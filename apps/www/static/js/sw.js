const CACHE = "anikude_sw_cache";
const PRE_CACHE = [
    /* Add an array of files to precache for your app */
    '/offline/',
    '/base.css',
    '/static/web/js/base.js',
    '/static/web/js/vendor/jquery/jquery.min.js',
    '/static/web/js/fontawesome/all.min.js',
    '/static/web/js/bootstrap/bootstrap.bundle.min.js',
    '/static/web/css/bootstrap/bootstrap.min.css',
    '/static/web/css/fontawesome/all.min.css',
    '/static/images/logo_sq.svg',
    '/static/images/logo_192_ro.png',

    '/static/web/css/owl.theme.default.min.css',
    '/static/web/css/mmenu.css',
    '/static/web/css/owl.carousel.min.css',
    '/static/web/js/owl.carousel.min.js',
    '/static/web/js/mmenu.polyfills.js',
    '/static/web/js/mmenu.js',
];

// TODO: replace the following with the correct offline fallback page i.e.: const OFFLINE_FALLBACK_PAGE = "offline.html";
const OFFLINE_FALLBACK_PAGE = "/offline/";

const FORCE_NETWORK_PATHS = [
  /* Add an array of regex of paths that should go network first */
  // Example: /\/api\/.*/
//   /\/.*/,
];

const DO_NOT_CACHE_PATHS = [
    /\/profile\/.*/,
    /\/user_profile\/.*/,
    /\/user\/.*/,
    /\/search\/.*/,
    /\/static\/images\/cache\/user\/.*/,
];

// Install stage sets up the offline page in the cache and opens a new cache
self.addEventListener("install", function (event) {
  console.log("[PWA] Install Event processing");

  event.waitUntil(
    caches.open(CACHE).then(function (cache) {
      console.log("[PWA] Cached offline page during install");

      return cache.addAll(PRE_CACHE);
    })
  );
});

// If any fetch fails, it will show the offline page.
self.addEventListener("fetch", function (event) {
  if (event.request.method !== "GET") return;

  event.respondWith(
        fetch(event.request).catch(function (error) {
        // The following validates that the request was for a navigation to a new document
            return caches.open(CACHE).then(function (cache) {
                
                return cache.match(event.request).then(response => {
                    if(response){
                        return response
                    }else{
                        return Promise.reject('not found')
                    }
                }).catch(error => {
                    console.error("[PWA] Network request Failed. Serving offline page " + error);
                    return cache.match(OFFLINE_FALLBACK_PAGE);
                })
        });
    })
  );
});

// This is an event that can be fired from your page to tell the SW to update the offline page
self.addEventListener("refreshOffline", function () {
  const offlinePageRequest = new Request(OFFLINE_FALLBACK_PAGE);

  return fetch(OFFLINE_FALLBACK_PAGE).then(function (response) {
    return caches.open(CACHE).then(function (cache) {
      console.log("[PWA] Offline page updated from refreshOffline event: " + response.url);
      return cache.put(offlinePageRequest, response);
    });
  });
});