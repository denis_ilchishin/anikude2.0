
$(document).ready(function(){
    $('.anikude_img_field_pic').on('click', function(){
        $('#image_browser').data('image', $(this));
        $('#image_browser').load('/admin/filebrowser/', function(){
            $('#image_browser').modal();
        });
    });

    $('#image_browser .__link').on('click', function(){
        if($(this).hasClass('__folder')){
            $('#image_browser').load('/admin/filebrowser/' + $(this).attr('href'), function(){
                $('#image_browser').modal();
            });
        }else{
            $('#image_browser').load($(this).attr('href'), function(){
                $('#image_browser').modal();
            });
        }
        
        return false;
    });

   $('#image_browser .thumbnail').on('click', function(event){
        image = $('#image_browser').data('image');
        image.find('img').attr('src', $(this).find('img').attr('src'));
        image.find('input').val($(this).find('input').val());
        $('#image_browser').modal('hide');
    });

    $('.anikude_img_field_reset').on('click', function(){
        img = $(this).parents('.anikude_img_field').find('.anikude_img_field_pic img');
        img.attr('src', img.attr('data-url'));
        $(this).parents('.anikude_img_field').find('.anikude_img_field_input').val(img.attr('data-value'));            
    });

    $('.anikude_img_field_remove').on('click', function(){
        img = $(this).parents('.anikude_img_field').find('.anikude_img_field_pic img');
        console.log(img)
        img.attr('src', img.attr('placeholder'));
        $(this).parents('.anikude_img_field').find('.anikude_img_field_input').val('');            
    });

});